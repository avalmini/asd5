#include <iostream>
#include <fstream>

using namespace std;

void load(int** &tab,int** &wynik,int &n,int &m,int &p){
    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("dane.txt");
    wyjscie.open("out.txt");
    wejscie>>n>>m>>p;
    tab = new int*[m];
    for(int i = 0; i < m; i++){
        tab[i] = new int[3];
    }
    wynik = new int*[p];
    for(int i = 0; i < p; i++){
        wynik[i] = new int[m];
    }
    for(int i=0;i<p;i++)
        for(int j=0;j<m;j++)
            wynik[i][j]=-1;
    for(int i=0;i<p;i++)
        wynik[i][0]=0;

    for(int i=0;i<m;i++){
        for(int j=0;j<3;j++){
            wejscie>>tab[i][j];
        }
    }
}

int partition(int** &tab, int p, int r){
    //int x = tab[p];
    int x[3];
    for(int i=0;i<3;i++)x[i]=tab[p][i];
    int i = p, j = r, w;
    while (true){
        while (tab[j][0] > x[0]){
            j--;
        }
        while (tab[i][0] < x[0]){
            i++;
        }
        if (i < j){
            for(int a=0;a<3;a++){w = tab[i][a];
            tab[i][a] = tab[j][a];
            tab[j][a] = w;
            }
            i++;
            j--;

        }
        else
            return j;
    }
}

void quicksort(int** &tab, int p, int r){
    int q;
    if (p < r){
        q = partition(tab,p,r);
        quicksort(tab, p, q);
        quicksort(tab, q+1, r);
    }
}

int alg(int** &tab,int** &wynik, int n, int m, int p){
    for(int i=0;i<m;i++){
        if(tab[i][2]==0)continue;
        if(wynik[0][tab[i][1]]==-1)wynik[0][tab[i][1]]=tab[i][2];
        else if(wynik[0][tab[i][1]]>wynik[0][tab[i][0]]+tab[i][2])wynik[0][tab[i][1]]=wynik[0][tab[i][0]]+tab[i][2];
    }
}

void output(int** tab,int** &wynik,int n,int p){
    for(int i=0;i<n;i++){
        for(int j=0;j<3;j++)
            cout<<tab[i][j]<<" ";
    cout<<endl;
    }
    cout<<endl;
    cout<<endl;
    for(int i=0;i<p;i++){
        for(int j=0;j<n;j++)
            cout<<wynik[i][j]<<" ";
    cout<<endl;
    }
}

int main()
{
    int n,m,p;
    int** tab;
    int** wynik;
    load(tab,wynik,n,m,p);
    quicksort(tab,0,m-1);
    output(tab,wynik,m,p);
    alg(tab,wynik,n,m,p);
    return 0;
}
