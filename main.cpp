#include <iostream>
#include <fstream>
#include <queue>
#include <algorithm>

using namespace std;

struct apex {
  int distance;
  int edge;

  apex(int distance, int edge) : distance(distance), edge(edge){}

};

bool operator<(const apex& a, const apex& b){
    return a.distance < b.distance;
}

void load(int** &tab,int** &dist,int &n,int &m,int &k){
    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("sredni1_in.txt");
    wyjscie.open("out.txt");
    wejscie>>n>>m>>k;
    tab = new int*[m];
    for(int i = 0; i < m; i++){
        tab[i] = new int[3];
    }
    dist = new int*[k];
    for(int i = 0; i <= k; i++){
        dist[i] = new int[m];
    }
    for(int i=0;i<=k;i++)
        for(int j=0;j<m;j++)
            dist[i][j]=INT_MAX;
    for(int i=0;i<=k;i++)
        dist[i][0]=0;

    for(int i=0;i<m;i++){
        for(int j=0;j<3;j++){
            wejscie>>tab[i][j];
        }
    }
}

int partition(int** &tab, int p, int r){
    //int x = tab[p];
    int x[3];
    for(int i=0;i<3;i++)x[i]=tab[p][i];
    int i = p, j = r, w;
    while (true){
        while (tab[j][0] > x[0]){
            j--;
        }
        while (tab[i][0] < x[0]){
            i++;
        }
        if (i < j){
            for(int a=0;a<3;a++){w = tab[i][a];
            tab[i][a] = tab[j][a];
            tab[j][a] = w;
            }
            i++;
            j--;

        }
        else
            return j;
    }
}

void quicksort(int** &tab, int p, int r){
    int q;
    if (p < r){
        q = partition(tab,p,r);
        quicksort(tab, p, q);
        quicksort(tab, q+1, r);
    }
}

int alg(int** &tab,int** &dist, int n, int m, int k){// n - miasta, m - korytarze, k - tunele
    int visited_count = 0,min_dist = INT_MAX,index, previous[n],x;
    bool visited[n];
    previous[0]=0;
    for(int i=0;i<n;i++){
        dist[0][i] = INT_MAX;
        visited[i] = false;
    }
    dist[0][0] = 0;
    while(visited_count != n){
        for(int i=0;i<n;i++){
            if(visited[i] != true && dist[0][i]<min_dist){
                min_dist = dist[0][i];
                index = i;
            }
        }
        visited[index] = true;
        for(int i=0;i<m;i++){

            if(tab[i][0]-1 == index ){
                if(dist[0][tab[i][0]-1] + tab[i][2] < dist[0][tab[i][1]-1]){
                    dist[0][tab[i][1]-1] = dist[0][tab[i][0]-1] + tab[i][2];
                    previous[tab[i][1]-1] = index+1;
                }
            }
            if(tab[i][1]-1 == index ){
                if(dist[0][tab[i][1]-1] + tab[i][2] < dist[0][tab[i][0]-1]){
                    dist[0][tab[i][0]-1] = dist[0][tab[i][1]-1] + tab[i][2];
                    previous[tab[i][0]-1] = index+1;
                }
            }
        }
        visited_count++;
        min_dist = INT_MAX;
    }



    cout<<dist[0][n-1]<<endl;
    x = n;
    while(x != 1){
        cout<<x<<" ";
        x = previous[x-1];
    }
    cout<<x<<endl;
}

void output(int** tab,int** &dist,int n,int m,int k){
    for(int i=0;i<m;i++){
        for(int j=0;j<3;j++)
            cout<<tab[i][j]<<" ";
    cout<<endl;
    }
    cout<<endl;
    cout<<endl;
    for(int i=0;i<=0;i++){
            for(int j=0;j<n;j++)
                cout<<dist[i][j]<<" ";
    cout<<endl;
    }
}

int main()
{
    int n,m,k;
    int** tab;
    int** dist;
    load(tab,dist,n,m,k);
    quicksort(tab,0,m-1);
    alg(tab,dist,n,m,k);
    //output(tab,dist,n,m,k);
    return 0;
}
